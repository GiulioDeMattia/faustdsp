import("stdfaust.lib");
atk= hslider("atk",0.3, .1, 4, 0.01);
hold= hslider("hold",.3, .1, 4, 0.01);
rel= hslider("release",.3, .1, 4, 0.01);
thresh= hslider("thresh",-30, -80, -10, 0.1);
gain = hslider("gain", 100, 0, 500, .1)/100.0 : si.smoo;
out = hslider("out", 0, 0, 400, .1)/100.0 : si.smoo;
f = hslider("f-osc", 44, 0, 50, .1) : si.smoo;
gain2 = hslider("out-osc", 0, 0, 100, .1)/100.0 : si.smoo;


oscino = (os.osc(f)/(2))*(gain2);

period = 0.5;
envelopeRelease = fi.highpass(6,60) : an.abs_envelope_rect(period);

in = si.bus(2) : *(gain), *(gain);
mid = +/sqrt(2);
side = -/sqrt(2);
//process = _ , _ <: _ , _ , mid , side ;


//  DEVANZTUNG SPACE
fdownoscFunc = oscAlone
with{
    oscAlone = (os.osc(f+ramp)/(2))*(gain2);
    ramp = AMPglissando*(1-(ba.period(FREQglissando)/(FREQglissando-1) : fi.lowpass(1,50))); 
    FREQglissando = ma.SR*12;
    AMPglissando = 12;
};
//process = fdownoscFunc;





//  PROVA PER ELIMINARE IL L'ATK
//process = mid <:(ef.gate_mono(thresh,atk,hold,rel): de.delay(ma.SR*2, ma.SR): envelopeRelease ), (ef.transpose(512,256,-72) ) :> _*(gain) ;

//  SVELANDO LO SPAZIO CON FASCE SINUSOIDALI
//process = oscino;

//  PER NONO
//process = in : + <: (ef.transpose(512,256,-72) * (fi.highpass(12,200) : (an.rms_envelope_tau(1)))*(out)); 

//  ESECUTORE NEGA LO SPAZIO CHE SI SVELA?
//process = in : + : fi.highpass(12,300) <: ((ef.transpose(1024,512,-72) * (an.rms_envelope_tau(.3)))*(out)) + (1 - (an.rms_envelope_tau(.1)*80))*fdownoscFunc; 

process = in : + : fi.highpass(12,300) <: ((ef.transpose(1024,512,-72) * (an.rms_envelope_tau(.2)))*(out)) + (1 - (an.rms_envelope_tau(.1)*40))*oscino; 


//  reazione AL BATTITO RMS
//process = _ : (1 - (an.rms_envelope_tau(.1)*80));