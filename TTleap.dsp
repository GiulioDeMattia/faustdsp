import("stdfaust.lib");
import("ISP.lib");

apf(t,g,x) = (x+_ : *(-g) <: _+x,_ : @(t-1),_)~(0-_) : mem+_;

apfn(0,t,g) = _;
apfn(1,t,g) = apf(t,g);
apfn(n,t,g) = apf(t,g) <: _,apfn(n-1,t,g);

msLeap(m,s) = m, sdmx(m,s),s <: par(i,4,apfn(4,1,1/sqrt(2))) : si.bus(13), 0-_ , 0-_ , _;

vstin = si.bus(2), si.block(14);
process = vstin : msLeap;