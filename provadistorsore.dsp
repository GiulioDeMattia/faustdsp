import("stdfaust.lib");

amp = hslider("amp tanh", 24, 0.1, 30, 0.1) : si.smoo;
//amp = 21;
//g = .9;
//t = 1;
g = hslider("Fdbk Gain", .7, -.999, 0.999, 0.001) : si.smoo;
t = hslider("samples", 1, 1, 1000, 1) : si.smoo;
interpolazione = hslider("t interp", 1.2, 0.1, 10, 0.1);


freqPhasor = 440;
counter = freqPhasor/ma.SR;
saw = ((_ <: + ~ (_+counter) % 1)-.5)*0.5;

oscil = os.osc(670);
//delay length interpolation
inter = ma.SR*interpolazione;
funDistort = ma.tanh ;
CombIIR(t) = _ <: (_+_*(1-abs(g)) ~ de.sdelay(ma.SR,inter,t-1)*g ): mem;
CombFIR(t) = _ <: _'+de.sdelay(ma.SR,inter,t);
fM = hslider("freq mod", 100,1,1000,1):si.smoo;

process = (oscil *amp*os.osc(fM)) : funDistort*.8 : CombIIR(t) <: si.bus(2);
//process = (oscil *amp) : funDistort*.8 : CombIIR(t) <: si.bus(2);
//process = (oscil *amp) : funDistort <: CombFIR(t)*(sqrt(2)/3) <: si.bus(2);
//process = (saw *amp) : funDistort*.8 : CombIIR(t) <: si.bus(2);
//process = (saw *amp) : funDistort : CombFIR(t)*(sqrt(2)/3) <: si.bus(2);

//process = saw : funDistort;
