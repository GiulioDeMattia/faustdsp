import("stdfaust.lib");

p= hslider("Polar Pattern",0,-100,100,0.1)/100;

process = os.lf_triangle(600)*0.2 : fi.allpass_comb((2^10),1024,p) : _*0.2 <: _,_; 