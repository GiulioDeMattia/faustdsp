import("stdfaust.lib");
radius= hslider("circle radius meters", 1, 0.5, 10, 0.1): si.smoo;
delCampioni= ((radius-radius*cos(ma.PI/4))/343.4)*ma.SR;


vmic(i) = _*cos(rad(i))+_*sin(rad(i)) <: de.delay(ma.SR*5,delCampioni)*treshold(i), _*(1-treshold(i)) :> + //
with{
    treshold(i)=(i+1)%2;
    rad(i) = (0-45*i)*ma.PI/180;
};
    
//vstin=si.bus(2),si.block(6);
vstin=os.osc(5000),os.osc(5000),si.block(8);

process=vstin<:par(i,8,vmic(i));