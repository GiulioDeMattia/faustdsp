import("stdfaust.lib");

//      0°
//  a   |   b
//    \ | /
//    / | \
//  g   |   d
//     180°

theta = 64.04;
alpha = theta / 2;
beta = 0-alpha;
gamma = 180-alpha;
delta = 0-gamma;
vmic(x) =((_*cos(rad(x)) + _*sin(rad(x))))
with{
    rad(x) = (x)*ma.PI/180;
};
vstin = si.bus(2), ! , !;
process = vstin <: vmic(alpha), vmic(beta), vmic(gamma), vmic(delta);