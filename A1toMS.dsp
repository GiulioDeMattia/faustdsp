import("stdfaust.lib");

pp= (0-hslider("Polar Pattern",50,1,100,1))/100:si.smoo;

routingBus= ro.crossn1(3): _, _,_,!;

ambix1toMS(m,omni,s) = mid(m,omni), side(s)
with{
    mid(m,omni) = omni*(1-pp)+m*pp;
    side(s)= s;
};
vstin=si.bus(4);
process=vstin: routingBus : ambix1toMS , 0 , 0;
