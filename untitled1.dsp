// import Standard Faust library 
// https://github.com/grame-cncm/faustlibraries/ 
import("stdfaust.lib");

op(x0) = filter ~ _
    with{ 
        filter(x1) = x00
            with{
                x00 = x0 * 0.0001 + x1 * 0.9999;
            };
        };
soglia = hslider("S", 0.01, 0.01, 0.99, 0.001);
process = os.phasor(1, 1000) > soglia <: op, _;