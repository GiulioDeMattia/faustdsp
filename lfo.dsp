import("stdfaust.lib");
declare author "Giulio";
freq=hslider("Frequency", 10, 0.5, 30, 0.5):si.smoo;
process=_*os.osci(freq),_*os.osci(freq);