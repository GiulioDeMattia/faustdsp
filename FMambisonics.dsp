declare name "FMimplemented in ambix1";
declare vendor "Hulio";
import("stdfaust.lib");

maxdelAp = 2^(hslider("MaxDelAp",3,1,10,1));
intdel = hslider("intDelAp",0,0,1000,1);
aN = hslider("anAp",99,1,99,1)/100:si.smoo;

hpf = hslider("hp freq",1000,0,10000,1):si.smoo;

bipolare = hslider("Bipolar-e",100,1,100,1)/100:si.smoo;
smootha = hslider("Smooth-a",100,1,100,1)/100:si.smoo;
smoothe = hslider("Smooth-e",100,1,100,1)/100:si.smoo;
rada=(0-hslider("DegOffset-a",1,-180,180,1))*ma.PI/180:si.smoo;
rade=(0-hslider("DegOffset-e",1,-180,180,1))*ma.PI/180:si.smoo;
gain = hslider("gain",0,0,.99,0.01):si.smoo;
ampPorta = hslider("ampPort-a",180,-100,500,0.05):si.smoo;
ampPorte = hslider("ampPort-e",180,-100,500,1):si.smoo;
fp = hslider("portant freq",440,-100,500,1):si.smoo;
fa = hslider("azymuth freq",3.39,-300,500,0.01):si.smoo;
fe = hslider("elevation freq",-1.39,-300,500,0.01):si.smoo;
ffa = hslider("freqMod azymuth",-84.22,-200,100,0.01):si.smoo;
ffe = hslider("freqMod elevation",-0.85,-200,100,0.01):si.smoo;
imod1= hslider("imoda",-.59,-70,50,0.01):si.smoo;
imod2= hslider("imode",.21,-70,50,0.01):si.smoo;
pp = hslider("Polar Pattern",100,1,100,1)/100:si.smoo;
a = os.osc(fa+os.osc(ffa)*imod1)*ampPorta+rada <: _*(1-smootha)+((_:si.smoo)*smootha);
e = os.osc(fe+os.osc(ffe)*imod2)*ampPorte+rade <: _*(1-smoothe)+((_:si.smoo)*smoothe);
hpm = hslider("hp-m",1,1,100,1)/100:si.smoo;
hps = hslider("hp-s",1,1,100,1)/100:si.smoo;
apm = hslider("ap-m",1,1,100,1)/100:si.smoo;
aps = hslider("ap-s",1,1,100,1)/100:si.smoo;
//------------------------------
//sdmx
sums=+/sqrt(2);
difs=-/sqrt(2);
sdmx = si.bus(2) <: sums,difs;
//------------------------------
// AMBIX
// first order
a0 = 1;
a1(a,e) = sin(a)*(cos(e)*(1-bipolare)+((cos(e)+1)/2)*bipolare);
a2(e) = sin(e);
a3(a,e) = cos(a)*(cos(e)*(1-bipolare)+((cos(e)+1)/2)*bipolare);
// mono to first order
mto1o(a,e) = _ <: _*a0,
                  _*a1(a,e),
                  _*a2(e),
                  _*a3(a,e);
//=============================



//------------------------------
ambix1toMS(m,omni,s) = mid(m,omni), side(s)
with{
    mid(m,omni) = omni*(1-pp)+m*pp;
    side(s)= s;
};
ambix1toLR(hpm,hps)= ambix1toMS : msFilterOperations
with {
    msFilterOperations =    _,_ <: hp ,ap :
                            _, ro.cross(2), _ :
                            _*(1-apm)+_*(apm), _*(1-aps)+_*(aps)
    with { 
        hp(m,s) =   (m :fi.highpass(1,hpf)*hpm), 
                    (s : fi.highpass(1,hpf)*hps), 
                    m*(1-hpm),s*(1-hps) : 
                    _, ro.cross(2), _ 
                    : +, +;

        ap =        (_ : fi.allpass_comb(maxdelAp,intdel,aN)), 
                    (_ : fi.allpass_comb(maxdelAp,intdel,aN));
    };
};

//------------------------------
//process = os.osc(fp)*gain : mto1o(a,e) : si.bus(2),si.block(1), _ : ro.crossn1(2) : ambix1toLR(hpm,hps);
//process = maxdelAp;
bamodulex(a0,a1,a2,a3) = lfu(a0,a1,a2,a3),rfd(a0,a1,a2,a3),rbu(a0,a1,a2,a3),lbd(a0,a1,a2,a3)
    with{
    lfu(a0,a1,a2,a3) = (a0 + a1 + a2 + a3)/2;
    rfd(a0,a1,a2,a3) = (a0 - a1 - a2 + a3)/2;
    rbu(a0,a1,a2,a3) = (a0 - a1 + a2 - a3)/2;
    lbd(a0,a1,a2,a3) = (a0 + a1 - a2 - a3)/2;
  };
//---------------------------------------


process = os.osc(fp)*gain : mto1o(a,e) : bamodulex;
