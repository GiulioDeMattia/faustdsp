import("stdfaust.lib");
import("MS.lib");

gain = hslider("gain", 0, 0, 100, .1)/100.0 : si.smoo;



oscaler4(mix) = *(1),
                 par(i,3,*(min(1,mixmm(mix)))),
                 par(i,5,*(max(0,(min(2,mixmm(mix))-1)))),
                 par(i,7,*(max(0,(min(3,mixmm(mix))-2)))),
                 par(i,9,*(max(0,mixmm(mix)-3)))
                 with{
                     mixmm(mix) = mix : max(0) : min(4);
                 };


process=_;
