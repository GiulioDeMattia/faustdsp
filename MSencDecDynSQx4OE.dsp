declare filename "MSencDecDynSQx4OE.dsp";
declare name "MSencDecDynSQx4OE";
import("stdfaust.lib");
declare author "Giulio";

//MONOTOMIDSIDE
pp= hslider("Polar Pattern",50,1,100,1)/100:si.smoo;
rad=(0-hslider("Degree",0,-180,180,1))*ma.PI/180:si.smoo;
msEnc(rad,x)= omni(x), mid(rad,x), side(rad,x)
with{
omni(x)= x;
mid(rad,x)=x*cos(rad);
side(rad,x)=x*sin(rad);
};


vmic(i) = (_*(1-pp)) + (pp*((_*cos(rad(i)) + _*sin(rad(i)))))
with{
    rad(i) = ((-90*i)-45)*ma.PI/180;
};
vstin=si.bus(1),si.block(3);
//-------
//test
//import("myneed.lib");
//vstin = os.osc(1000) : mspan(pp,rad);
//process=os.osc(1000) , (vstin<:par(i,4,vmic(i)));// : ro.crossn1(3) : si.bus(3), _ : _,_,ro.cross(2));
//-------
process = vstin : msEnc(rad) <: par(i,4,vmic(i)) : ro.crossn1(3) : si.bus(3), _ : _, _, ro.cross(2);