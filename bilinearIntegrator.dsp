import("stdfaust.lib");

dirac = 1-1';
// frequency cut off
w = 440;
bilinearIntegrator(w) = _ * (w/2) <: + ~ _ <: _+_';
process = dirac : bilinearIntegrator(w);
