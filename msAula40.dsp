import("stdfaust.lib");

pp= hslider("Polar Pattern",50,0,100,1)/100:si.smoo;

vmic(i, offset, pp, m, s) = mid_dec(m,pp,rad(i,offset)) + side_dec(s,rad(i,offset))
    with{
        rad(i,offset) = ((90 * i) + offset) * ma.PI/180;
        mid_dec(m,pp,rad) = ((m * (1 - pp)) + (m * cos(rad) * (pp)));
        side_dec(s,rad) = (s * sin(rad));
    };


vstin = si.bus(2),si.block(6);
//test
//vstin = os.osc(440) <: _, 0;
//process = os.osc(440), (vstin <: par(i, 4, vmic(i,-45)) , par(i, 4, vmic(i,0)));
process = vstin <: par(i, 4, vmic(i,-45,pp)) , par(i, 4, vmic(i,0,pp));