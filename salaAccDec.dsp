import("stdfaust.lib");

deg1 = 0;
deg2 = 50*ma.PI/180;
deg3 = ma.PI/2;
deg4 = (32+90)*ma.PI/180;
deg5 = ma.PI;
deg6 = (180+56)*ma.PI/180;
deg7 = 3*ma.PI/2;
deg8 = (270+38)*ma.PI/180;
a1 = _ *cos(deg1) + _ * sin(deg1);
a2 = _ *cos(deg2) + _ * sin(deg2);
a3 = _ *cos(deg3) + _ * sin(deg3);
a4 = _ *cos(deg4) + _ * sin(deg4);
a5 = _ *cos(deg5) + _ * sin(deg5);
a6 = _ *cos(deg6) + _ * sin(deg6);
a7 = _ *cos(deg7) + _ * sin(deg7);
a8 = _ *cos(deg8) + _ * sin(deg8);

process = _ , _ , si.si.block(6) <: a1, a2, a3, a4, a5, a6, a7, a8;