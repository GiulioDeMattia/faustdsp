import("stdfaust.lib");

xy=(0-hslider("XY",0,-180,180,1))*ma.PI/180:si.smoo;
zx=(0-hslider("ZX",0,-180,180,1))*ma.PI/180:si.smoo;
zy=(0-hslider("ZY",0,-180,180,1))*ma.PI/180:si.smoo;


xyAxisRotation(a0,a1,a2,a3) = a0, a1Turn(a1), a2, a3Turn(a3)
with{
    a3Turn(a3) = a3*cos(xy) + a3*sin(xy);
    a1Turn(a1) = a1*sin(xy) + a1*cos(xy);
};

zxAxisRotation(a0,a1,a2,a3) = a0, a1, a2Turn(a2), a3Turn(a3)
with{
    a3Turn(a3) = a3*cos(zx) + a3*sin(zx);
    a2Turn(a2) = a2*sin(zx) + a2*cos(zx);
};

zyAxisRotation(a0,a1,a2,a3) = a0, a1Turn(a1), a2Turn(a2), a3
with{
    a1Turn(a1) = a1*cos(zy) + a1*sin(zy);
    a2Turn(a2) = a2*sin(zy) + a2*cos(zy);
};

//a0, a1, a2, a3
vstin = si.bus(4);

process = vstin : xyAxisRotation : zxAxisRotation : zyAxisRotation;