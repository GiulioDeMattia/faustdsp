import("stdfaust.lib");

fdbck = _ : + ~ g
with{
    g= _*.9;
};

A = out ~ _
with{
    out(y, x) = y + x;
};

B = out ~ (_ <: si.bus(2))
with{
    out(y, x, z) = y + x + z;
};

process= fdbck;