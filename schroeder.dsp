import("stdfaust.lib");

//il feedback ~ occupa di per se un campione;

//t = 1;
//g=0.998;
g = hslider("gain", 0, -1, 1, .001);
t = hslider("tau", 1, 1, 1000, 1);
f = hslider("freq", 1, .001, 20, .001);
//delay in feedback loop
dfl = +@(t-1)~ *(g) : mem;
//process=ba.pulsen(1,ma.SR):dfl;
dfld(g,t) = ( + : de.delay(1000,(t-1)))~ *(g) : mem;
//all-pass filter
apf(g,t) = _ <: _*(-g) + dfld(g,t)*(1-(g*g));
//all-pass reverberator UN ALL-PASS DENTRO UN ALL-PASS
//process=ba.pulsen(1,ma.SR):apf(g,t);
dfldapf(g,t) = ( + : de.delay(1000,(t-1) : apf(1,t)))~ *(g) : mem;

apfrev(g,t) = _ <: _*(-g) + dfldapf(g,t)*(1-(g*g));
//process=ba.pulsen(1,ma.SR):dfldapf(g,t);
process=dfl;
gosc(f)=os.osci(f);
//process = gosc(400) : apf(g,823) : apf(g,2699) : apf(g,7823);
//process = par(i,2,ba.pulsen(1,ma.SR/256) : apf(gosc(f),823) : apf(gosc(f),2699) : apf(gosc(f),7823) : apf(gosc(f),10607) : apf(gosc(f),19391));
//process = ba.pulsen(1,ma.SR) : apfrev(gosc(f),823);

/*
freeverb = _<:  _ + (dfld(.6,1579),
                dfld(.7,1709), 
                dfld(.8,4639), 
                dfld(.9,7331) :> apf(.7,1873) : apf(.7,109))*1;
*/
//  
/*
dritto = _<:  dfld(.6,1579),
                dfld(.7,1709), 
                dfld(.8,4639), 
                dfld(.9,7331) :> apf(.7,1873) : apf(.7,109);

rovescio = apf(.7,1873) : apf(.7,109) <: dfld(.6,1579),
                dfld(.7,1709), 
                dfld(.8,4639), 
                dfld(.9,7331) :>_;

process= ba.pulsen(1,ma.SR) <: rovescio, dritto; */