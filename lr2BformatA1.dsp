import("stdfaust.lib");
import("EncDec.lib");
//ENCODER FROM LR TO BFORMAT AMBIX-1
vGroup(x) = vgroup("A1param",x);
e = vGroup(hslider("elevation",0,-180,180,0.1)*(ma.PI/180));
pp= vGroup(hslider("Polar Pattern",50,1,100,1)/100:si.smoo);

process = si.bus(2), si.block(2) : lr2A1;