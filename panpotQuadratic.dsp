import("stdfaust.lib");
//slider [0 to 1]
pan= hslider("panpot", 0.5, 0, 1, 0.01);
//------------
//Quadratic panning to prevent -3dB when panpot is 0.5 .
//Scalar panner that covers with amplitude difference between channels.
//------------
panL=_*sqrt(1-pan);
panR=_*sqrt(pan);

vstin= os.osci(440);
//vstin= _;
process=vstin <: panL, panR;