import("stdfaust.lib");
import("ISP.lib");

gain = hslider("gain Out", 2, 0, 6, 0.01) : si.smoo;

// ==============================================================================
// panner mid side

rad = 0-hslider("[1]degrees", 0,-180,180,1)*ma.PI/180 : si.smoo;
pp = hslider("[2]polar pattern", 50 , 0 , 100, 1)/100 : si.smoo;

mspan(rad,pp,in) = mid(rad,pp,in),side(rad,in)
    with{

        mid(rad,pp,in) = in*(1-pp) + in*cos(rad)*pp;
        side(rad,in) = in*sin(rad);
    };


//------------------------------------------- DELAY FEEDBACK IN LOOP - FIXED ---
// delay compensation, right impulse response
dflc(t,g) = (+ : @(t-1))~*(g) : mem;
//TEST
//process = dfl(1,.7);

//========================================================== ALL-PASS FILTER ===
apf(t,g) = _ <: _*(-g)+ dflc(t,g)*(1-(g*g));
//TEST
//process = apf(1,.7);

// INCREASE OF ECHO DENSITY
// fig. 3
apfSeries(t,g,N) = seq(i,N,apf(t*((1/3)^i),g));


// reverberation time
gainRev = .3;
T = 1;
tau(x) = ba.sec2samp(x);
t1 = tau(0.04);
t2 = tau(0.0424);
t3 = tau(0.036);
t4 = tau(0.0387);
gNth(tNth) = 10^(((-3*tNth)/(T*ma.SR)));
combApf = _ <: _ +
                (dflc(t1,gNth(t1)),
                dflc(t2,gNth(t2)),
                dflc(t3,gNth(t3)),
                dflc(t4,gNth(t4)) 
                :> apfSeries(0.004,.7,2)*gainRev);




//================================================ AMBIOPHONIC-REVERBERATION ===
//---------------------------------------------------------- NESTED ALL-PASS ---
apfn(0,t,g) = _;
apfn(1,t,g) = apf(t,g);
apfn(n,t,g) = apf(t,g) <: _,apfn(n-1,t,g);


// ================================================================================================
// ----tracciamento--------------
peakenv = abs(_) : max ~ _*.995;


// ================================================================================================
// -------gate-------------------
gate = _ > .5;
// smorzatore per inviluppo impulsivo
s = 1;
fc = 1/s;
G = tan(ma.PI*fc/ma.SR);
j = G/(1+G);
integrator = _*(j) : + ~ _*(1-j);
// TEST
//process = 1-1' : gate : integrator;

// ================================================================================================
// ------evento positivo---------
gi = hslider("fb nested", 0.3, 0.001, 0.999, 0.001) : si.smoo;
g = hslider("fb", 0.999, 0.001, 0.999, 0.001) : si.smoo;
freqTrainPulse = hslider("[0]freq", 30, 1, 1000, 1) : si.smoo;
//gi=0.5;
sampsnested = hslider("sampsNested", 10, 1, 1000, 1);
lp = _*(1-gi) : + ~ (de.sdelay(ma.SR, 2*ma.SR, sampsnested-1)*gi);
samps = hslider("samples", 10, 1, 1000, 1);
//g=.999;
combiir(x) = _*(1-g) : + ~ (de.sdelay(ma.SR, 2*ma.SR, x-1) : lp: _*g);
b= no.noise : combiir(90);
//TEST
//process = no.noise : combiir(samps);
//process= vgroup("osctrainiir",ba.pulsen(1,ma.SR/freqTrainPulse) : combiir(samps) *.5),
//        vgroup("osctrainiir2",ba.pulsen(1,ma.SR/freqTrainPulse) : combiir(samps) *.5);

// da aggiustare
process= ba.pulsen(1,ma.SR/freqTrainPulse) : mspan(rad,pp) : vgroup("mid",combiir(samps)) *.5,vgroup("side",combiir(samps)) *.5 : combApf, combApf : sdmx ;

//process= ba.pulsen(1,ma.SR/freqTrainPulse) <: vgroup("L",combiir(samps)) *.5,vgroup("R",combiir(samps)) *.5 : apfn(4,2459,.7),apfn(4,2963,.7);


// ================================================================================================
input2 = _, _ : ro.cross(2) : _ , !;

//process = input2 : (peakenv : gate : integrator) * b : _* gain ;