import("stdfaust.lib");

vbarM = attach(_,abs : ba.linear2db : vbargraph("h:RM/v:MID/[0]Input[style:numerical]",-60,0));
// - MID -
choiceFreqMid = hslider("h:RM/v:MID/[1]freq m[style:menu{'100-1000':0;'1-100':1;'1.-10.':2}]",0,0,2,1);
freq1 = hslider("h:RM/v:MID/[2]freq M", 100, 100, 1000, 1) : si.smoo;
lowfreq1 = hslider("h:RM/v:MID/[3]low freq M", 1, 1, 100, 1) : si.smoo;
verylowfreq1 = hslider("h:RM/v:MID/[4]very low freq M", 1, 1, 1000, 1)/100 : si.smoo;
dw1 = vslider("h:RM/v:MID/[5]DRY/WET[style:knob]", 100, 0, 100, 1)/100 : si.smoo;
// - SIDE - 
vbarS = attach(_,abs : ba.linear2db : vbargraph("h:RM/v:SIDE/[0]Input[style:numerical]",-60,0));
choiceFreqSide = hslider("h:RM/v:SIDE/[1]freq s[style:menu{'100-1000':0;'1-100':1;'1.-10.':2}]",0,0,2,1);
freq2 = hslider("h:RM/v:SIDE/[2]freq S", 100, 100, 1000, 1) : si.smoo;
lowfreq2 = hslider("h:RM/v:SIDE/[3]low freq S", 1, 1, 100, 1) : si.smoo;
verylowfreq2 = hslider("h:RM/v:SIDE/[4]very low freq S", 1, 1, 1000, 1)/100 : si.smoo;
dw2 = vslider("h:RM/v:SIDE/[5]DRY/WET[style:knob]", 100, 0, 100, 1)/100 : si.smoo;


freqMid  = freq1, lowfreq1, verylowfreq1 : select3(choiceFreqMid);
freqSide = freq2, lowfreq2, verylowfreq2 : select3(choiceFreqSide);
rmMid  = os.osc(freqMid ) * _;
rmSide = os.osc(freqSide) * _;

rmInsideMS(m,s) =   ( m <: vbarM <: m * (1 - dw1) + rmMid * (dw1)), 
                    ( s <: vbarS <: s * (1 - dw2) + rmSide * (dw2));

process = rmInsideMS;

//process =   ( _ <: vbar <: _ * (1 - dw) + rM * (dw)),
//            ( _ <: vbar <: _ * (1 - dw) + rM * (dw));