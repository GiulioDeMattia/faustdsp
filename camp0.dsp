import("stdfaust.lib");
import("ISP.lib");

gain= hslider("gain",50,0,200,1)/200:si.smoo;

twopi = 2*ma.PI;
omega(fc) = fc*twopi/ma.SR;
// BI-LINEAR TRANSFORM
aapp(fc) = pow(ma.E, 0-omega(fc));
//TEST
//process = aapp(100);

// Low Pass One Pole
//
// g -> filter coeff.
lp1p(g) = *(1-g):+~*(g);
//TEST
//process = no.pink_noise : lp1p(.5);

// INTEGRATORE
//  average of absolute values of input samples, 
//  over specific time windows (one may try RMS measures, instead)
//
// s -> seconds
integrator(s) = abs : lp1p(aapp(1/s));
//TEST
//process = os.impulse :  integrator(0.01);


// DELAY IN FEEDBACK LOOP --   TEST SIMLAZIONE CONTROLLO NON SAMPLE RATE
//delayFbk(s,fbk) = (+ : de.delay(ma.SR/5, ba.sec2samp(s)))~(si.smooth(ba.tau2pole((1/ma.SR)*64))*(fbk));
// DELAY IN FEEDBACK LOOP
delayFbk(s,fbk) = (+ : de.delay(ma.SR, ba.sec2samp(s)))~*(fbk);
//process = os.impulse : delayFbk(0.001,0.89);

//process = os.impulse : integrator(0.004) :  delayFbk(0.0001,0.94) : mapQ;
// MAP-Q
mapQ(x) = (1-x)^2;
//process = mapQ(0.5);

// MAP-L
mapL(x) = (1-x);
//process = mapL(0.5);

//'''Background Noise Study: SIGNAL FLOW 1 (network of control signals)'''
// --  APPUNTI 
// parametro feedback sensato solo in relazione al materiale di ingresso come 
//  ''ea3''
//inAmp0 = integrator(0.01) : delayFbk(0.001,0.79) : mapQ;

// valori:
// - fdbk = 0.0001 , 0.94
// - delay at = 0.01
// - integrator ig = 0.01
camp0(at,ig) = _ <: de.delay(ma.SR,ba.sec2samp(at)) * (integrator(ig) : delayFbk(0.0001,0.94) : mapQ);
//camp0(at,ig)=_<:de.delay(ma.SR,ba.sec2samp(at))*(integrator(ig):delayFbk(10,0.98):mapQ);



//camp1(at) = de.delay(ma.SR/5,ba.sec2samp(at)) * (integrator(at) : delayFbk(0.001,0.79) : mapL);
//process = par(i,2,fi.highpass(2,100)) :>  inAmp0;
//process = no.noise*0.1 : inAmp0;
//myinput = si.block(8),si.bus(2);
mic1 = (camp0(0.01,0.01) : fi.dcblockerat(20));
mic2 = (camp0(0.01,0.01) : fi.dcblockerat(20));

//process = mic1 ;//, mic2 ;
/*process = camp0(0.01,0.01),
          camp0(0.01,0.01) : par(i,2,fi.dcblockerat(20));*/




//========================================================= MOORER FUNCTIONS ===
//==============================================================================
sjm = library("seam.moorer.lib");
//------------------------------------------------------- all-pass fig. 1(a) ---
//apf(t,g,x) = (x+_ : *(-) <: _+x,_ : @(t-1),_)~(0-_) : +; // ir sbagliato
//apf(t,g,x) = (x+_ : *(-g) <: _+x,_ : @(t-1),_)~(0-_) : mem+_; // ir corretto
apf(t,g,x) = (x+_ : *(-g) <: _+x,_ : de.delay(ma.SR,t-1),_)~(0-_) : mem+_;
// process = os.impulse : apf(1,0.7);
//
//----------------------------------------------------------- comb fig. 2(a) ---
//comb(t,g) = (+ : @(t-1))~*(g) : mem;
comb(t,g) = (+ : de.delay(ma.SR,t-1))~*(g) : mem;
//process = os.impulse : comb(1,0.9);
//
//---------------------------------------- n-channel ambiophonic all-pass matrix
//------------------------------------------------- (non presente nell'articolo)
apfn(0,t,g) = _;
apfn(1,t,g) = apf(t,g);
apfn(n,t,g) = apf(t,g) <: _,apfn(n-1,t,g);
//process = ba.pulsen(1,ma.SR) : apfn(32,2,1/sqrt(2)); // esempio a 16 uscite
//
//----------------------------------------------------- oscillating all-pass ---
apfo(0,t,g,x) = x;
apfo(1,t,g,x) = apf(t,g,x);
apfo(n,t,g,x) = (x+_ : *(-g) <: _+x,_ : apfo(n-1,t,g),_ : de.delay(ma.SR,t-1),_)~(0-_) : mem+_;
//process = ba.pulsen(1,ma.SR) <: apfo(2,3,1/sqrt(2)),apfo(2,5,-1/sqrt(2));


process = sdmx : 
          (apfo(1,2,1/sqrt(2)) : mic1), // MID
          (apfo(2,13,1/sqrt(2)) : mic1) : // SIDE
          fi.lowpass(4,750), // MID
          fi.lowpass(2,2500): // SIDE
          sdmx;