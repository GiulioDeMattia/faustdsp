import("stdfaust.lib");
integrator = + ~ _;
slidingSum(n) = integrator <: _, _@int(max(0,n)) :> -;
slidingMean(n) = slidingSum(n)/n;
avg_rect(period, x) = x : slidingMean(rint(period * ma.SR));
abs_envelope_rect(period, x) = abs(x) : avg_rect(period);
n = 0.01;
d(soglia,x) = x > soglia : + ~ _ ;
process = _ <:_ , abs_envelope_rect(n), (abs_envelope_rect(n) : d(0.4));