import("stdfaust.lib");

xy=(0-hslider("XY",0,-180,180,1))*ma.PI/180:si.smoo;

pTurn(rad,axis1,axis2)=axis1*cos(rad) + axis1*sin(rad);

rotate = pTurn(xy);
vstin = _,_;
process = vstin : rotate ;  