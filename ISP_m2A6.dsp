declare name "ISP mono to AMBIX VI";
declare vendor "SMERM";

import("ISP.lib");

//param input
a = 0-hslider("azymuth",0,-180,180,0.1)*(ma.PI/180);
e = hslider("elevation",0,-180,180,0.1)*(ma.PI/180);
pp = hslider("polarpattern",50,0,100,1)/(100):si.smoo;

// TEST
//process = os.osc(1000) : mto4o(a,e);
// VST
process = _,si.block(49) : mto6o(a,e,pp), 0;
// MAX
//process = mto6o;