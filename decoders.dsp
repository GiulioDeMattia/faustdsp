import("stdfaust.lib");

////====================================================================///
// library of decoders for various quadraphonic MidSide configurations
////====================================================================///

//========================================================
//----------Left-front Right-front Left-back Right-back---
//
//      _____________________
//      ==== decX4oe4545 ====
//      ^^^^^^^^^^^^^^^^^^^^^
//
//          ~ ODD - EVEN ~
//          ''''''''''''''
//          1(lf)   2(rf)
//
//          3(lb)   4(rb)
//
/////////-----------
//
//      _____________________
//      ==== decX4oe4545 ====
//      ^^^^^^^^^^^^^^^^^^^^^
//
//          ~ DIAMOND ~
//          '''''''''''
//          1(lf)   2(rf)
//
//          4(lb)  3(rb)
//-------------------------------------------------------




//======================================================
//-----------------quadrato di gerzon------------------
//
//       _______________________
//       ==== gerzonDiamond ====
//       ^^^^^^^^^^^^^^^^^^^^^^^
//
//               (Mid)
//                 1 
//                  
//      (left) 4       2 (right)
//
//                 3 
//               (side)
//
//
//------------------------------------------------------




//======================================================
//---quadrifonia a quadrato di gerzon ma con altro algoritmo---
//
//      ______________________
//      ==== decX4Diamond ====
//      ^^^^^^^^^^^^^^^^^^^^^^
//
//                1 
//                  
//            4       2
//
//                3
//
//
//-----------------------------------------------------------






p(x) = hgroup("GERZON decoder Diamond",x);
pp = hslider("polarPattern OMNI-0 FIG.8-100", 50, 0, 100, 1)/100 : si.smoo;
bal = p(hslider("balance: MID -1, SIDE +1", 0, -1, 1, 0.001));
nsum(x,y) = (x+y)/sqrt(2);
ndif(x,y) = (x-y)/sqrt(2);
sdmx(x,y) = nsum(x,y), ndif(x,y);
vstin = si.bus(4) : si.bus(2), si.block(2);





/////////////////////////////////////////////////////////
// 0 -> (-90) -> (-180) -> (-270)
bal2rad = (((bal+1)/2)*90)*ma.PI/180;
balance = _*cos(bal2rad), _*sin(bal2rad);

dec4Gerzon(mid,side) = mid,side <: mid, sdmx , side : _, ro.crossn1(1),_ : _, _ , ro.cross(2);
//process = vstin : balance : dec4Gerzon;




/////////////////////////////////////////////////////////
// 0 -> (-90) -> (-180) -> (-270)
micvMS(i,m,s) = m*cos(rad)*pp + m*(1-pp) + s*sin(rad)
    with{
        rad = (0-i*90)*ma.PI/180;
    };

decLRfb(mid,side) = mid,side <: par(i, 4, micvMS(i));
//process = vstin : decLRfb;




/////////////////////////////////////////////////////////
// 45 -> (-45) -> (-135) -> (-225)
micMS(i,m,s) = m*cos(rad)*pp + m*(1-pp) + s*sin(rad)
    with{
        rad = (+45-i*90)*ma.PI/180;
    };

decLRfbDiamond(mid,side) = mid,side <: par(i, 4, micMS(i));
//process = vstin : decLRfbDiamond;






/////////////////////////////////////////////////////////
// 45 -> (-45) -> (135) -> (-135)
micv(deg,m,s) = m*cos(rad)*pp + m*(1-pp) + s*sin(rad)
    with{
        rad = deg * ma.PI/180;
    };

dec4lrFB= si.bus(2) <: micv(45),micv(-45),micv(135),micv(-135);
//process = vstin : dec4lrFB;