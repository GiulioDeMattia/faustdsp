import("stdfaust.lib");


vmic(deg) = _*cos(rad)+_*sin(rad)
    with{
        rad = deg * ma.PI/180;
    };

process = si.bus(2), !,! <: vmic(45), vmic(0-45), vmic(90+45), vmic(-90-45);