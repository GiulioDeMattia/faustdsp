import("stdfaust.lib");
declare author "Giulio"; 
declare name "MSdecx10";
N=10;
vmic(i) = _*cos(rad(i))+_*sin(rad(i))
with{
    rad(i) = (-i*(360/N))*ma.PI/180;
};
vstin=si.bus(2),si.block(N-2);
nMix = (vstin <: par(i,N,vmic(i)));
process=nMix;