import("stdfaust.lib");

fc=10000;
//fc = hslider("freq cut off", 20, 1, 47999,1);
//s = vslider("Signal[style:radio{'Lp':0;'Hp':1}]",0,0,1,1);
s = 1;
//fc = 47000;
//G = g(tan(ma.PI*fc/ma.SR));

pole(s,fc,x) = x : _ + _ * (1 - abs(G(fc,s))) ~ (_ * G(fc,s));
    with{
    G(fc,s) = g(cos((ma.PI*(ma.SR/2-fc)/ma.SR))/sin((ma.PI*(ma.SR/2-fc)/ma.SR))), -g(tan(ma.PI*fc/ma.SR)) : select2(s);
    g(x) = x/(1+x);
    };
process = (no.noise : pole);