import("stdfaust.lib");

gainRM = hslider("GainRM",0,0,1,0.01):si.smoo; 
gainDO = hslider("GainDirectOut",0,0,1,0.01):si.smoo; 
gainDEL = hslider("GainDelay",0,0,1,0.01):si.smoo; 
freq = hslider("freq",440,0,500,1):si.smoo;
s = hslider("RM[style:menu{'Sine':0;'Triangle':1;'Sawtooth':2}]", 0, 0, 3, 1);
g = hslider("gain", 0, -1, 1, .001);
t = hslider("tau", 1, 1, 1000, 1):si.smoo;
f = hslider("freq", 1, .001, 20, .001);

//delay in feedback loop
dfl = de.sdelay(ma.SR*10,1024,t) + _ ~ *(g);

RM = (os.osc(freq) , os.triangle(freq), os.sawtooth(freq) : select3(s) : _), _ : *;



process = _ <: RM*gainRM, _*gainDO, dfl*gainDEL :> _;
