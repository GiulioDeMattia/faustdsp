import("stdfaust.lib");
declare author "Giulio";
vmic(i) = _*cos(rad(i))+_*sin(rad(i))
with{
    rad(i) = (-45-90*i)*ma.PI/180;
};
dec=par(i,4,vmic(i));
vstin=si.bus(2),si.block(2);
process=vstin<:dec;