import("stdfaust.lib");
declare author "Giulio";
vmic(i) = _*cos(rad(i))+_*sin(rad(i))
with{
    rad(i) = (-i*90)*ma.PI/180;
};
dec=par(i,4,vmic(i));
vstin=si.bus(2),si.block(2);
//process= os.osc(1000), (vstin <: par(i,4,vmic(i)));
process= vstin <: dec;