declare name "ISP LR to AMBIX I ORDER";
declare vendor "SMERM";

import("ISP.lib");
a0g = hslider("[01]Omni", 0, 0, 100, 1)/100 : si.smoo;
a2g = hslider("[02]Elevation",0,-180,180,0.1)*(ma.PI/180);

//LR2MS2AMBIX1 static without any process with a 8-figure mid
static = sdmx : ro.cross(2) :  0 , _ , 0 , _;

mspola1(m,s) = m <: *(a0g), s, *(sin(a2g)), *(cos(a2g));
//process = mpolar;

lr2a1 = sdmx <: mspola1;
vstin = _, _, !, !;
process = vstin : lr2a1;