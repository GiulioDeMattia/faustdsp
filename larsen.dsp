import("stdfaust.lib");
import("ISP.lib");
gain = hslider("Input Gain", 1, 0, 3, 0.01) : si.smoo;
g = hslider("Fdbk Gain", .7, 0.001, 0.999, 0.001) : si.smoo;
//g= 0.9;
msec = hslider("msec integrator", 1,0.5, 20, 0.01) : si.smoo;
//msec = 0.05;
//FC = 1000/msec;
//FC = 10;
fc = hslider("frequency cut-off", 20, 20, 1000, 1) : si.smoo;
FC1 = hslider("frequency cut-off1", 20, 20, 1000, 1) : si.smoo;

//In = 1-1';
//In = _;
In = _,_;
//In = no.noise*.4;

//process = In : OneZero;
//process = In : OnePoleSimple;
//process = In <: OneZero, OnePoleSimple;
//process = In <: OnePoleSimple, OneZero;
//process = no.noise*.2 : Hp1pBT;

//process = In : 
//                sdmx :
//            fi.highpass(2,200) ,
//            fi.highpass(2,200) : 
//            tgroup("Mid-Lar", OnePoleBT) ,
//            tgroup("Side-Lar", OnePoleBT) :
//                sdmx;

G = tan(ma.PI*fc/ma.SR);
K = G/(1+G);
L = 1/(1+G);

onePoleBt(x) = x : ( _ + _*K ) ~ (_*L);

del= hslider("delay samps", 20, 1, 10000,1);
del1= hslider("delay 1", 200, 1, 10000,1);
del2= hslider("delay 2", 20, 1, 10000,1);
fdel= hslider("delay freq", 20, 1, 10000,1) : si.smoo;
interp=ma.SR*10;
f= hslider("freq", 20, 1, 1000,1) : si.smoo;
amp = hslider("amp fM", 1, 1, 100, 1) : si.smoo;

dellen = 1/(fdel*ma.SR);
comb = _ : _+_*(1-g) ~ (_ <: de.delay(ma.SR,int(del1-1))*dw, de.delay(ma.SR,int(del2-1))*(1-dw) :> _*g) : mem;
combiir = _ : _+_*(1-g) ~ de.sdelay(ma.SR,interp,int(del))*g;
combiir1 = _ : (_*os.osc(f))+_*(1-g) ~ de.sdelay(ma.SR,interp,int(del))*g;
combiir2 = _ : (_*os.osc(f+(amp*os.osc(f))))+_*(1-g) ~ de.sdelay(ma.SR,interp,int(del))*g;
process =   In : 
            fi.highpass(2,100),
            fi.highpass(2,100) : 
            tgroup("Left-Larsen",comb),
            tgroup("Right-Larsen",comb) :fi.dcblockerat(40),fi.dcblockerat(40);