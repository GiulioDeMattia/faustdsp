import("stdfaust.lib");
index= hslider("index",3.6, 3.5, 4, 0.001);
x= 0.5;

logmap(x) = index*(x-x*x) ;
process = x * os.impulse:  +~ (logmap):fi.highpass(1,1)<: _,_;
