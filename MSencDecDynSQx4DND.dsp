import("stdfaust.lib");
declare author "Giulio";

//MONOTOMIDSIDE
pp= hslider("Polar Pattern",50,0,100,1)/100:si.smoo;
rad=(0-hslider("Degree",0,-180,180,1))*ma.PI/180:si.smoo;
msPseudoStereo(rad,x)= x, mid(rad,x), side(rad,x)
with{
mid(rad,x)=x*cos(rad);
side(rad,x)=x*sin(rad);
};


vmic(i,w,mid,side) = (w*(1-pp)) + (pp*((mid*cos(rad(i)) + side*sin(rad(i)))))
with{
    rad(i) = (((0-90)*i)-45)*ma.PI/180;
};
dec(w,mid,side)=par(i,4,vmic(i,w,mid,side));
vstin=si.bus(1),si.block(3);
//-------
//test
//import("myneed.lib");
//vstin = os.osc(1000) : mspan(pp,rad);
//process=os.osc(1000) , (vstin<:par(i,4,vmic(i)));
//-------
process = vstin : msPseudoStereo(rad) <: dec; 