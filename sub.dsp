import("stdfaust.lib");

gain = hslider("gain", 0, 0, 100, .1)/100.0 : si.smoo;
f = hslider("f-osc", 0, 0, 50, .1) : si.smoo;

in1 = os.osc(f)/(2);

process = in1*(gain);