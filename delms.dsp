import("stdfaust.lib");
t = hslider("tau", 1, 1, 10000, 0.1);
g = hslider("gain", 0, -1, 1, .001);

dfl = +@((t*ma.SR/1000)-1)~ *(g) : mem;

process = _ , _ : dfl, dfl;