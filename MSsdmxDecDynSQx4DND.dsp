import("stdfaust.lib");
declare author "Giulio";
import("ISP.lib");
//LRtoMS
pp= hslider("Polar Pattern",50,0,100,1)/100:si.smoo;

vmic(i,mid,side) = (mid*(1-pp)) + (pp*((mid*cos(rad(i)) + side*sin(rad(i)))))
with{
    rad(i) = (((0-90)*i)-45)*ma.PI/180;
};
vstin=si.bus(2),si.block(2);
dec(mid,side) = par(i,4,vmic(i,mid,side));
//-------
//TEST
//vstin = os.osc(1000), 0-os.osc(1000)*0.8;
//process=os.osc(1000), (vstin : sdmx <: dec);
//-------
process = vstin <: sdmx <: dec;
