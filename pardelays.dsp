import("stdfaust.lib");

g = hslider("gain", 0, -1, 1, .001);


//delay in feedback loop
dfl(i) = de.sdelay(ma.SR*10,1024,val(i)) + _ ~ *(g)
    with{
    val(i) = i*ma.SR;
    };

process = _ <: par(i,10, dfl(i)) : (par(i, 10, _ * hslider("gain %i", 0.5, 0, 1, 0.01):si.smoo))  :> _*(checkbox("aio"):si.smoo);
//process=par(i, 10, hslider("gain %i", 0, 0, 1, 0.01):si.smoo) ;