import("stdfaust.lib");
declare author "Giulio";

phaseoscGroup(x) = hgroup("phaseOsc",x);
oscGroup(x) = hgroup("FreqOscPhaseDryWet",x);

freq=hslider("Freq Carrier",30,0,10000,1):si.smoo;
foscfphase=phaseoscGroup(hslider("Freq Osc Phase",30,0.1,10000,1):si.smoo);
freqPhaseOsc = phaseoscGroup(hslider("Amp PhaseOsc", 20,1,10000,1):si.smoo);
ampOscOsc=oscGroup(hslider("Amp Osc",300,0,10000,1):si.smoo);
ampNoiseOscOScOSc= oscGroup(hslider("Amp Noise",30,0,10000,1):si.smoo);

fphosc = os.osci(foscfphase);

randDryWet= oscGroup(vslider("rand-DryWet[style:knob]",0.5,0,1,0.01):si.smoo);
ain=os.oscp(freq,os.osci((fphosc*freqPhaseOsc)*(1-randDryWet)+(os.osci(((no.noise+1)/2)*ampNoiseOscOScOSc)*ampOscOsc)*randDryWet));

process = ain*checkbox("On/off"),si.block(2)<: _,_: fi.dcblocker, fi.dcblocker;