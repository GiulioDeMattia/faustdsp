import("stdfaust.lib");
//slider
rad= 0-(hslider("azymuth", 0, -180,180,1)*ma.PI/180);
//---------------
//Sum-difference of mid Side to create eight-figure 
//  coincidents Blumlein stereophonic pair.
//Stereo width 360° --- encoding ---
//---------------
panL=(cos(rad)+sin(rad))/sqrt(2);
panR=(cos(rad)-sin(rad))/sqrt(2);

//vstin= os.osci(440);
vstin= _;

process=vstin<: _*panL, _*panR;