
import("stdfaust.lib");
//param input
a = 0-hslider("azymuth",0,-180,180,0.1)*(ma.PI/180);
e = hslider("elevation",0,-180,180,0.1)*(ma.PI/180);
pp= hslider("Polar Pattern",50,0,100,1)/100:si.smoo;

eDec = hslider("eDec",0,-180,180,0.1)*(ma.PI/180);


a0 = 1;
a1(a,e) = sin(a)*cos(e);
a3(a,e) = cos(a)*cos(e);
// second order
a4(a,e) = sqrt(3/4)*sin(2*a)*(cos(e))^2; 
a5(a,e) = sqrt(3/4)*sin(a)*sin(2*e);
a7(a,e) = sqrt(3/4)*cos(a)*sin(2*e);
a8(a,e) = sqrt(3/4)*cos(2*a)*(cos(e))^2;
// third order


mto2o(a,e) = _ <: _*a0,
                  _*a1(a,e),
                  _*a3(a,e),
                  _*a4(a,e),
                  _*a5(a,e),
                  _*a7(a,e),
                  _*a8(a,e);

vmic2(a,e,pp) = _*a0*(1-pp) + (pp*(_*a1(a,e) +_*a2(e) +_*a3(a,e) +_*a4(a,e) +_*a5(a,e) +_*a7(a,e) +_*a8(a,e)))/2;

lrDec(eDec) = par(i,2,vmic2(ma.PI/4+i*(ma.PI*3/2),eDec,pp));

process = no.pink_noise : mto2o(a,e) <: lrDec(eDec);