import("stdfaust.lib");

//samps = hslider("numero di comb", 1, 0, 1000, 1);
samps = 40;
CombFIR(samps,x) = x + x@(samps);

CombIIR(samps,x) = x : _ + (_) ~ _@(samps-1)) * g;

dirac = 1-1';
process = no.noise * 0.1 :CombFIR(samps);