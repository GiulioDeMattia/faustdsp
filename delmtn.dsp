import("seam.lib");

delmtn(N) = par(i,N,de.delay(ma.SR/10,sma.imt2samp(mt(i))))
with{
    mt(c) = hslider("%f distance",0,0,30,0.1)
    with{
      f = c+1;
    };
};
process = delmtn(8);