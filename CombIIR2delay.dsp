import("stdfaust.lib");

declare name "CombIIR2delay";
declare author "Giulio";
declare version "1.00";


tot(x) = hgroup("CombIIR",x);
g = tot(vslider("[1] fb gain", .7, 0.001, 0.999, 0.001) : si.smoo);

dw = tot(vslider("[4]drywet", 0, 0, 1, 0.001): si.smoo);


// first par delay
del1= tot(vslider("[2]del1 [unit:smpl]", 200, 1, 1000,1));

// second par delay
del2= tot(vslider("[3]del2 [unit:smpl]", 20, 1, 1000,1));

vumeterIN = _ <: attach(_,abs : ba.linear2db : tot(vbargraph("[0]Level In[unit: dB]",-60,0)));
vumeterOUT = _ <: attach(_,abs : ba.linear2db : tot(vbargraph("[5]Level Out[unit: dB]",-60,0)));

comb = _ : _+_*(1-g) ~ (_ <: de.delay(ma.SR,int(del1-1))*(1-dw), de.delay(ma.SR,int(del2-1))*(dw) :> _*g) : mem;

process = vumeterIN : comb : vumeterOUT;