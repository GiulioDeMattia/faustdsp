import("DMfilters.lib");

//s = vslider("Signal[style:radio{'Lp':0;'Hp':1}]",0,0,1,1);
//fc = hslider("freq cut off", 20, 1, 48000,1):si.smoo;
choice = 1;
fc = 47999.9;
g(x) = x/(1+x);
G(fc,choice) =   g(cos((ma.PI*(fc)/ma.SR))/sin((ma.PI*(fc)/ma.SR))),
            -g(tan(ma.PI*fc/ma.SR)) :
            select2(choice);


lpHp1pBT(fc,choice,x) = x : _ + _ * (1 - abs(G(fc,choice))) ~ (_ * G(fc,choice));
process = (no.noise : lpHp1pBT(fc,choice)), G(fc,choice);

//fc = 40000;
//G = tan(ma.PI*fc/ma.SR);
K = G/(1+G);
L = 1/(1+G);

onePoleBt(x) = x : ( _ + _*K ) ~ (_*L);
//process = no.noise : onePoleBt;

gHp = -.999;
onePoleHighPass(x) = x : (_ + _*(1-abs(gHp))) ~ (_*gHp);
//process = no.noise : onePoleHighPass;