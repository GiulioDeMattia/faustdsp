import("stdfaust.lib");

pp = hslider("d", 0, 0, 1, 0.01): si.smoo;
gain = nentry("gain",1,1,4,0.01):si.smoo;
BPTPTNormalized(gf, bw, cf, x) = loop ~ si.bus(2) : (! , ! , _ * gf)
    with {
        g = tan(cf * ma.PI * ma.T);
        R = 1.0 / (2.0 * bw);
        G1 = 1.0 / (1.0 + 2.0 * R * g + g * g);
        G2 = 2.0 * R + g;
        loop(s1, s2) = u1 , u2 , bp * 2.0 * R
            with {
                hp = (x - s1 * G2 - s2) * G1;
                v1 = hp * g;
                bp = s1 + v1;
                v2 = bp * g;
                lp = s2 + v2;
                u1 = v1 + bp;
                u2 = v2 + lp;
            };
    };
lp1p(g) = *(1-g):+~*(g);
        twopi = 2*ma.PI;
        omega(fc) = fc*twopi/ma.SR;
        aapp(fc) = pow(ma.E, 0-omega(fc));

sdmx = si.bus(2) <: sums, difs
    with{
        sums = +/sqrt(2);
        difs = -/sqrt(2);
    };

mspan(pp,rad) = _ <: mid,side
    with{
        mid(in) = in * (1-pp) + (in * cos(rad))* pp;
        side(in) = in * sin(rad);
    };


// pseudo-random noise with linear congruential generator (LCG)
noise(initSeed) = lcg ~ _ : (_ / m)
with{
    a = 18446744073709551557; c = 12345; m = 2 ^ 31; 
    lcg(seed) = ((a * seed + c) + (initSeed - initSeed') % m);
};


fError = no.noise : ba.sAndH(ba.pulsen(1,ma.SR*.46));
apf(t,g) = _ <: *(-g)+(dflc(t,g)*(1-(g*g)));
articolazione(n,seed) = noise(seed) : ba.sAndH(ba.pulsen(1,ma.SR*.4)) : _^n :lp1p(aapp(1/(1.7)));
forma(n,seed) = noise(seed) : ba.sAndH(ba.pulsen(1,ma.SR*6)) : _^n :lp1p(aapp(1/(2)));
fun(n,seed) = articolazione(6,seed)^(n) / (n^2);
vibrato = os.osc((4+fError)+forma(3,4932)*3)*(3+fError+forma(3,932)*3);
freq1 = 800 + vibrato + articolazione(3,345)*20 + (forma(4,686)*520);
freq2 = 800 + vibrato + articolazione(3,9500)*20 + (forma(3,1400)*420);
Npartial=12;
dflc(t,g) = (+ : @(t-1))~*(g) : mem;
apfSeries(t,g,N) = seq(i,N,apf(t*((1/3)^i),g));
gainRev = .5;
T = 6;
tau(x) = ba.sec2samp(x);
t4 = tau(0.0424);
t3 = tau(0.0416);
t2 = tau(0.036);
t1 = tau(0.0329);
gNth(tNth,tSec) = 10^(((-3*tNth)/(T*ma.SR)));
combApf(tSec,gainRev) = _ <: _ +
                (dflc(t1,gNth(t1,tSec)),
                dflc(t2,gNth(t2,tSec)),
                dflc(t3,gNth(t3,tSec)),
                dflc(t4,gNth(t4,tSec)) 
                :> apfSeries(0.004,.7,2)*gainRev);


corde(freq,seed) = (par(i,Npartial, BPTPTNormalized(10,700*(i+1)+300*articolazione(2,seed) ,freq*(i+1))) : par(i,Npartial,_*fun((i+1),seed)) :>_ : fi.lowpass(2,freq*Npartial):*(gain) : min(1));
//process = articolazione(3);
process = no.multinoise(Npartial*2) :   (corde(freq1,345) : mspan(1, forma(3,4380)*180) : _, combApf(2,.1+forma(2,3360)*.3): sdmx),
                                        (corde(freq2+12,9500) :mspan(1,forma(3,5360)*180) : _ , combApf(2,.1+forma(2,7360)*.4) : sdmx)
                                        :>_,_ ;
//process = par(i,Npartial,fun((i+1)));
//process = fError; 