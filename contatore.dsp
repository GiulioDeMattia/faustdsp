import("stdfaust.lib");
//counter
//f(x)= x<x';
//time =1/ma.SR;
//phasor = time : + ~ _%1;
//process = phasor : f : + ~ _;

//---------------------------
//peak for each samples
//d(x) = x > 0.8;
//----
//peak for mean of y samples
meanSamps = 96;
takingDelayedSamp(y) = + ~ @(y);
d(y,x) = (x <: par(i, y,takingDelayedSamp(i+1)):> _/y) > 0.8;
process = _ : d(meanSamps) : + ~ _ ;

//now version in milliseconds
// problems with par and his parameter, must to be an integer constant value, bah
//msecs = 1;
//meanSamps = (msecs*ma.SR)/1000 : int;
//takingDelayedSamp(y) = + ~ @(y);
//d(y,x) = (x <: par(i, y,takingDelayedSamp(i+1)):> _/y) > 0.8;
//process = _ : d(meansamps) : + ~ _ ;