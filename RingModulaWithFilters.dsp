import("stdfaust.lib");

// - MID -
vbarM = attach(_,abs : ba.linear2db : vbargraph("v:RM/h:sopra/h:Inputs/[0]Input",-60,0));
choiceFreqMid = hslider("v:RM/h:sopra/h:SEPARATI/v:MID/[1]freq m[style:menu{'100-1000':0;'1-100':1;'1.-10.':2}]",0,0,2,1);
freq1 = hslider("v:RM/h:sopra/h:SEPARATI/v:MID/[2]freq M", 100, 100, 1000, 1) : si.smoo;
lowfreq1 = hslider("v:RM/h:sopra/h:SEPARATI/v:MID/[3]low freq M", 1, 1, 100, 1) : si.smoo;
verylowfreq1 = hslider("v:RM/h:sopra/h:SEPARATI/v:MID/[4]very low freq M", 1, 1, 20, 0.01) : si.smoo;
dw1 = vslider("v:RM/h:sopra/h:SEPARATI/v:MID/h:outmid/[8]DRY/WET[style:knob]", 100, 0, 100, 1)/100 : si.smoo;

// - SIDE - 
vbarS = attach(_,abs : ba.linear2db : vbargraph("v:RM/h:sopra/h:Inputs/[1]Side",-60,0));
choiceFreqSide = hslider("v:RM/h:sopra/h:SEPARATI/v:SIDE/[1]freq s[style:menu{'100-1000':0;'1-100':1;'1.-10.':2}]",0,0,2,1);
freq2 = hslider("v:RM/h:sopra/h:SEPARATI/v:SIDE/[2]freq S", 100, 100, 1000, 1) : si.smoo;
lowfreq2 = hslider("v:RM/h:sopra/h:SEPARATI/v:SIDE/[3]low freq S", 1, 1, 100, 1) : si.smoo;
verylowfreq2 = hslider("v:RM/h:sopra/h:SEPARATI/v:SIDE/[4]very low freq S", 1, 1, 20, 0.01) : si.smoo;
dw2 = vslider("v:RM/h:sopra/h:SEPARATI/v:SIDE/h:outside/[8]DRY/WET[style:knob]", 100, 0, 100, 1)/100 : si.smoo;

// - TOGETHER -
choiceFreq = hslider("v:RM/h:sotto/v:UNITI/[1]freq m[style:menu{'100-1000':0;'1-100':1;'1.-10.':2}]",0,0,2,1);
freq = hslider("v:RM/h:sotto/v:UNITI/[2]freq M", 100, 100, 1000, 1) : si.smoo;
lowfreq = hslider("v:RM/h:sotto/v:UNITI/[3]low freq M", 1, 1, 100, 1) : si.smoo;
verylowfreq = hslider("v:RM/h:sotto/v:UNITI/[4]very low freq M", 1, 1, 20, 0.01) : si.smoo;
dw2gether = vslider("v:RM/h:sotto/v:UNITI/[8]DRY/WET[style:knob]", 100, 0, 100, 1)/100 : si.smoo;


dw = vslider("v:RM/h:sotto/h:outputs/[1]Uniti - Separati[style:knob]", 100, 0, 100, 1)/100 : si.smoo;

vbarout1 = attach(_,abs : ba.linear2db : vbargraph("v:RM/h:sotto/h:outputs/h:[2]SEPARATI/[0]Mid",-60,0));
vbarout2 = attach(_,abs : ba.linear2db : vbargraph("v:RM/h:sotto/h:outputs/h:[0]UNITI/[2]Mid",-60,0));
vbarout3 = attach(_,abs : ba.linear2db : vbargraph("v:RM/h:sotto/h:outputs/h:[2]SEPARATI/[1]Side",-60,0));
vbarout4 = attach(_,abs : ba.linear2db : vbargraph("v:RM/h:sotto/h:outputs/h:[0]UNITI/[3]Side",-60,0));

vbarMidDir = attach(_,abs : ba.linear2db : vbargraph("v:RM/h:sopra/h:SEPARATI/v:MID/h:outmid/direct[style:numerical]",-60,0));
vbarMid = attach(_,abs : ba.linear2db : vbargraph("v:RM/h:sopra/h:SEPARATI/v:MID/h:outmid/modified[style:numerical]",-60,0));
vbarSideDir = attach(_,abs : ba.linear2db : vbargraph("v:RM/h:sopra/h:SEPARATI/v:SIDE/h:outside/direct[style:numerical]",-60,0));
vbarSide = attach(_,abs : ba.linear2db : vbargraph("v:RM/h:sopra/h:SEPARATI/v:SIDE/h:outside/modified[style:numerical]",-60,0));
vbar2Dir = attach(_,abs : ba.linear2db : vbargraph("v:RM/h:sopra/h:SEPARATI/v:MID/direct[style:numerical]",-60,0));
vbar2 = attach(_,abs : ba.linear2db : vbargraph("v:RM/h:sopra/h:SEPARATI/v:MID/modified[style:numerical]",-60,0));


freq2gether = freq, lowfreq, verylowfreq : select3(choiceFreq);
freqMid  = freq1, lowfreq1, verylowfreq1 : select3(choiceFreqMid);
freqSide = freq2, lowfreq2, verylowfreq2 : select3(choiceFreqSide);

rm2gether  = os.osc(freq2gether) * _;
rmMid = os.osc(freqMid) * _;
rmSide = os.osc(freqSide) * _;

rmInsideMS(m,s) = (midAlone(m)*dw<:vbarout1) + (mid2gether(m)*(1-dw)<:vbarout2),
                  (sideAlone(s)*dw<:vbarout3) + (side2gether(s)*(1-dw)<:vbarout4)
        with{
            midAlone(m) = ( m <: vbarM <: ((_ * (1 - dw1))<:vbarMidDir) + ((rmMid * (dw1))<:vbarMid));
            sideAlone(s) = ( s <: vbarS <: ((_ * (1 - dw2))<:vbarSideDir) + ((rmSide * (dw2)) <: vbarSide));

            mid2gether(m) = ( m <: _ * (1 - dw2gether) + rm2gether * (dw2gether));
            side2gether(s) = ( s <: _ * (1 - dw2gether) + rm2gether * (dw2gether));
    };
/*rmInsideMS(m,s) =   ( m <: vbarM <: m * (1 - dw1) + rmMid * (dw1))*dw + ( m <: vbar <: m * (1 - dw2gether) + rm2gether * (dw2gether))*(1-dw),
                    ( s <: vbarS <: s * (1 - dw2) + rmSide * (dw2))*dw + ( s <: vbar <: s * (1 - dw2gether) + rm2gether * (dw2gether))*(1-dw) ;
*/
//test
//vstin = os.osc(300)*.1 , os.osc(300.1)*.1 ;
process = rmInsideMS;