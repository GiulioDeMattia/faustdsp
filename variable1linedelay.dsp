import("stdfaust.lib");

g = hslider("gain", 0, -1, 1, .001);
sec = hslider("seconds",0.001,0.001,10, 0.001):si.smoo;
f = hslider("freq", 1, .001, 20, .001):si.smoo;

//delay in feedback loop
dfl(i) = de.sdelay(ma.SR*10,1024,val(i)) + _ ~ *(g)
    with{
    val(i) = i*ma.SR;
    };

process = _ : dfl(sec) * (hslider("outgain", 0, 0, 1, 0.01):si.smoo);
