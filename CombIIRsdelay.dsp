import("stdfaust.lib");

declare name "CombIIRsdelay";
declare author "Giulio";
declare version "1.00";

g = hslider("Fdbk Gain", .7, 0.001, 0.999, 0.001) : si.smoo;
del= hslider("delay samps", 20, 1, 1000,1);
n = nentry("interpolation", 1, 1, 20,1);

interp = ma.SR*int(n);
combiir = _ : _+(_*(1-g)) ~ de.sdelay(ma.SR,interp,(del-1))*g : mem;
process = combiir;