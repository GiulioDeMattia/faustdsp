import("stdfaust.lib");

sums=+/sqrt(2);
difs=-/sqrt(2);
sdmx = sums,difs;
process=si.bus(2)<:sdmx;
