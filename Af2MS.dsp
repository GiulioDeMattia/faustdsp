import("stdfaust.lib");

pp= hslider("Polar Pattern",50,0,100,1)/100:si.smoo;
z= hslider("z gain",0,0,100,1)/100:si.smoo;


abmodulex(LFU,RFD,RBU,LBD) = a0,a1,a2,a3
	with{
    a0 = 0.5 * (LFU + RFD + RBU + LBD);
	  a1 = 0.5 * (LFU - RFD - RBU + LBD);
	  a2 = 0.5 * (LFU - RFD + RBU - LBD);
    a3 = 0.5 * (LFU + RFD - RBU - LBD);
};

ambix1toMS(omni,s,a2,m) = mid(omni,m,a2), side(s,a2)
with{
    mid(omni,m,a2) = omni*(1-pp)+m*pp + a2*z;
    side(s,a2)= s + a2*z;
};

vstin = si.bus(4);

process = vstin : abmodulex : ambix1toMS , 0 , 0;